import os

import pytest


from classeafile import app as myapp


def pytest_configure(config):
    os.environ["CLASSEAFILE_ROOT"] = "tests/storage"


@pytest.fixture
def app():
    return myapp


@pytest.fixture
def mock_http(monkeypatch):
    class Response:
        def __init__(self, status_code, content=b""):
            self.status_code = status_code
            self.content = content
            self.ok = status_code == 200

    def _(status_code=200, content=""):
        print("Patched")

        async def request(*args, **kwargs):
            print("Called")
            return Response(status_code, content)

        monkeypatch.setattr("requests_async.get", request)

    return _
