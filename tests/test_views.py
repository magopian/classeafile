import json
from pathlib import Path

import pytest

pytestmark = pytest.mark.asyncio


async def test_unauthorized_request_should_return_401(mock_http, client):
    mock_http(401)
    resp = await client.put("/file.pdf", body=b"")
    assert resp.status == 401
    assert resp.body == b"Remote status: 401"


async def test_not_relative_path_should_raise(mock_http, client):
    mock_http(401)
    resp = await client.put("/file/../../hackme.pdf", body=b"")
    assert resp.status == 400
    assert resp.body == b"Invalid path file/../../hackme.pdf"


async def test_basic_put(mock_http, client):
    mock_http(200)
    resp = await client.put("/dir1/dir2/file.txt", body=b"foofoo")
    assert resp.status == 204
    path1 = Path("tests/storage/dir1/dir2/file.txt")
    assert path1.exists()
    assert path1.read_text() == "foofoo"
    resp = await client.put("/dir1/dir3/file.txt", body=b"foofoo")
    assert resp.status == 204
    resp = await client.get("/dir1/dir2/")
    assert resp.status == 200
    assert json.loads(resp.body) == ["dir1/dir2/file.txt"]
    resp = await client.get("/dir1/")
    assert resp.status == 200
    assert json.loads(resp.body) == ["dir1/dir2/file.txt", "dir1/dir3/file.txt"]
    path1.unlink()
    path1.parent.rmdir()
    path2 = Path("tests/storage/dir1/dir3/file.txt")
    path2.unlink()
    path2.parent.rmdir()
    path2.parent.parent.rmdir()
