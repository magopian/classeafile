# Classy Files

## Install

In a venv:

    python setup.py develop

Set CLASSEAFILE_AUTH_URL env var, eg.:

    export CLASSEAFILE_AUTH_URL="https://peertube.space/api/v1/users/me"


## Usage

    classeafile serve

Test it with httpie:

    http PUT :4321/test/pouet.pdf @path/to/some.pdf
    http :4321/test

